# Self hosted GitLab & services

The [GitLab](https://gitlab.io) instance I (solely) setup, manage and provide support for.

## Talking points
* Instance history
* Maintenance
* Support
* Services:
    * git source control
    * GitLab pages (static sites)
    * Docker registry
    * GitLab runners CI/CD (run on the Kubernetes Cluster)
* 500 - 1000 active users. 
