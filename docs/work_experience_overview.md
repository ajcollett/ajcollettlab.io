Broad overview of responsibilities over the past 6 years with a spotlight on relevant items.

!!! note "tl;dr"
    * System Administration
    * Hardware and software technical setup and support.


## Talking points
My work focus has been the Sciences faculty, most of the work being at Computer Science.

### Assets - desktops, laptops & servers

Support for mainly the Computer Science and Applied Maths staff and postgraduate labs.

* Hardware procurement, management, monitoring and support.
* Linux (Ubuntu mainly) Desktops/laptops:
    * Setup, configuration, support.
    * 50+ assets
* Linux (Ubuntu) servers:
    * ~20 servers, some of which are desktop boxes.

* Some Mac support

### User facing services

These services have varying user-bases, GitLab being the most widely used.

* GitLab
* JupyterHub
* Documentation, course and other websites
* Mailing lists
* SSH jumphost
* Scripting
* Others
