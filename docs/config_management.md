# Software stack for orchestration and configuration management

The applications I use to manage the hardware, and services on the hardware.

## Talking points
* [iPXE](https://ipxe.org/) for booting live images and installation media.
* [SaltStack](https://saltproject.io/) for configuration management (used to use Puppet).
* [Ubuntu](https://ubuntu.com/) as OS on servers.
* [Rancher Kubernetes](https://www.rancher.com/products/rancher) on certain servers in the research cluster.