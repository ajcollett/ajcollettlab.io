
A new service I deployed last year for undergraduate courses and postgraduate research.

## Talking points
* Run on Kuberenetes
* Access for students to Computer Science compute resources
* Jupyter Notebooks, Code-Server and browser based terminal
* Customizable profiles