Here are a few links (some may not be accessible) to projects I setup or support.

* [System admin Docs website](https://system-administrator.pages.cs.sun.ac.za/)
* [Netboot](https://git.cs.sun.ac.za/system-administrator/netboot)
* [SaltStack](https://git.cs.sun.ac.za/system-administrator/saltstack/common)
* [Class list automation](https://git.cs.sun.ac.za/Computer-Science/tech-admin/class-list-automation)
* [Website building templates](https://git.cs.sun.ac.za/websites-hosts-admin)
* [GitLab scripts](https://git.cs.sun.ac.za/system-administrator/py-gitlab-scripts)
* [Fleet - k8s config](https://git.cs.sun.ac.za/system-administrator/k8s)