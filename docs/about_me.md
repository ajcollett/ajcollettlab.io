These are just talking points about me. 

## Talking points:
* Father, husband
* Where I grew up
* Curiosity/making things
* Studies
* Marriage and first job
* Where I live now and current stage of life
* Things that I enjoy spending time on:
    * Technical thought experiments
    * Finding solutions to problems that serve the group
    * Odd projects around the house
    * Being an active part in my sons growth
