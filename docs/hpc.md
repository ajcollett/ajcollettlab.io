# High Performance Computing

My experience with HPC and how I impliment this for my department.

## Talking points

* Brief overview of current knowledge.
* My relationship with main HPC.
* The server grade hardware I support currently:
    * Dell R740 x2
    * Nvidia [A100](https://www.nvidia.com/en-us/data-center/a100/) and [V100](https://www.nvidia.com/en-us/data-center/v100/)
    * Kubernetes backbone nodes: Dell Precision Rack form-factor.
    * Other Dell nodes.
* My software stack for HPC:
    * JupyterHub
    * SSH access
    * Rootless Docker
